package creationalPattern;

public final class Employee {

	// instance fields 
    private int id; 
    private String name; 
    private String address; 
  
    // Setter Methods 
    // Note that all setters method 
    // return this reference 
    public Employee emId(int id) 
    { 
        this.id = id; 
        return this; 
    } 
  
    public Employee setName(String name) 
    { 
        this.name = name; 
        return this; 
    } 
  
    public Employee setAddress(String address) 
    { 
        this.address = address; 
        return this; 
    } 
  
    @Override
    public String toString() 
    { 
        return "id = " + this.id + ", name = " + this.name +  
                               ", address = " + this.address; 
    } 

}

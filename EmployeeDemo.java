package creationalPattern;

public class EmployeeDemo {

	public static void main(String args[]) 
    { 
		Employee em1 = new Employee(); 
		Employee em2 = new Employee(); 
  
        em1.emId(1).setName("vino").setAddress("Polannaruwa"); 
        em2.emId(2).setName("vini").setAddress("Kandy"); 
  
        System.out.println(em1); 
        System.out.println(em2); 
    } 

}
